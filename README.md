﻿# Zork

Zork project by DUPONT Malo et CAUMES Kirian

## Lancer le programme

- Modifier la `ConnectionStrings` des appsettings.json dans le "DataAccessLayer" et "App"
- Démarrer le projet "App"
- S'amuser :p

## Comment jouer ?
Un menu "About" est disponible dans le menu principal, pour le reste tout est disponible à l'écran, suivez le guide !

## Les easter eggs dispo

Les easter eggs ne peuvent être enclenchés que dans le menu principal de la partie en cours

- taper **uuddlrlrba** pour revenir au maxHP (déclenche le Konami Code)
- taper **kill** pour se suicider
- taper **win** pour gagner

## Features en plus

- Présence d'indices-boussoles disséminés dans les salles (pour gagner, il faut trouver la salle-sortie)
- Stylisation des menus
- Codes de triche

## Autres

La taille de la grille de jeu par défaut est de 100x100 et peut être modifié dans le fichier appsettings.json
(propriété `GridSize`).

A noter que le jeu n'est pas très équilibré.

Ce jeu présente des traits d'humour pouvant heurter la sensibilité des plus jeunes. Faites attention.