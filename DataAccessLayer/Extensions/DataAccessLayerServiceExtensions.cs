﻿using DataAccessLayer.AccessLayer;
using DataAccessLayer.Context;
using Microsoft.Extensions.Configuration;

namespace StudentManager.DataAccessLayer.Extensions
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    
    public static class DataAccessLayerServiceExtensions
    {
        /// <summary>
        ///     Extension method use to initialize data access.
        /// </summary>
        /// <param name="services">Collection of the available services for the app.</param>
        /// <returns>Returns edited services collection.</returns>
        public static IServiceCollection AddDataAccessLayerService(this IServiceCollection services, IConfiguration test)
        {
            services.AddDbContext<ZorkDbContext>(options =>
            {
                options.EnableSensitiveDataLogging();
                
                options.UseSqlServer(test.GetConnectionString("Default"),
                    opt =>
                    {
                        opt.MigrationsAssembly("StudentManager.DataAccessLayer");
                    });
            });

            services.AddTransient<CellAccessLayer>();
            services.AddTransient<WeaponAccessLayer>();
            services.AddTransient<PlayerAccessLayer>();
            services.AddTransient<BonusAccessLayer>();
            services.AddTransient<MonsterAccessLayer>();

            return services;
        }
    }
}
