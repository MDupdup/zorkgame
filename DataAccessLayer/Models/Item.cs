﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Models
{
    public abstract class Item : BaseDataObject
    {
        [Required]
        public string Name { get; set; }
        
        [Required]
        [Column(TypeName = "ntext")]
        public string Description { get; set; }
    }
}