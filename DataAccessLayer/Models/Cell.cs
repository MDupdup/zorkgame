﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Models
{    
    [Table("Cells")]  
    public class Cell : BaseDataObject
    {
        [Required]
        [Column(TypeName = "ntext")]
        public string Description { get; set; }
        
        [Required]
        public int PosX { get; set; }
        
        [Required]
        public int PosY { get; set; }
        
        [Required]
        public int MonsterSpawnRate { get; set; }

        [Required]
        public bool IsWinningCell { get; set; }

        public Monster Monster { get; set; }


        public override string ToString()
        {
            string text = $"{Description}\n";

            // TODO update this
            if (Monster != null)
            {
                string desc = Monster.ToString();
                text += $"There is a monster here! \n{desc}";
            } 
            // else if ("" != null)
            // {
            //     string desc = Item.ToString();
            //     text += $"There is a weapon here:\n{desc}";
            // } 

            else {
                text += $"There is nothing here";
            } 
            return text;
        }
    }
}