﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Models
{    
    [Table("Players")]  
    public class Player : BaseDataObject
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int Xp { get; set; }
        
        [Required]
        public int MaxHp { get; set; }

        [Required]
        public int Hp { get; set; }
        
        [Required]
        public int BaseAtk { get; set; }
        
        [Required]
        public int BaseDef { get; set; }

        [NotMapped]
        public int Level => (int) Math.Floor((decimal) (Xp / 100));

        public Weapon Weapon { get; set; }
        
        public ICollection<Bonus> Bonuses { get; set; }
        
        public int CellX { get; set; }
        
        public int CellY { get; set; }
        
        public int MonsterKilled { get; set; }

        public Player()
        {
            Hp = MaxHp;
        }

        public override string ToString()
        {
            return $"Player {Name} has {Hp}/{MaxHp}HP, and {Xp}xp (lvl {Level})." +
                   $"\nPlayer base atk: {BaseAtk}" +
                   $"\nPlayer base def: {BaseDef}" +
                   $"\n{Name} has killed {MonsterKilled} monster(s).";
        }
    }
}