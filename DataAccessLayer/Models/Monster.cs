﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Models
{    
    [Table("Monsters")]  
    public class Monster : BaseDataObject
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int Attack { get; set; }
        
        [Required]
        public int Defense { get; set; }

        [Required]
        public int HpMax { get; set; }
        
        [Required]
        public int Hp { get; set; }

        [Required]
        public int MissRate { get; set; }

        [Required]
        [Column(TypeName = "ntext")]
        public string Description { get; set; }

        public Monster()
        {
            Hp = HpMax;
        }
        
        public override string ToString()
        {
            return $"{Name}: {Description}";
        }
    }
}