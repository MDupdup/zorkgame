﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DataAccessLayer.Models
{    
    [Table("Bonuses")]  
    public sealed class Bonus : Item
    {
        [Required]
        public int HpGain { get; set; }

        [Required]
        public int AtkGain { get; set; }

        [Required]
        public int DefGain { get; set; }

        public override string ToString()
        {
            return $"{Description}";
        }
    }
}