﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Models
{    
    [Table("Weapons")]  
    public sealed class Weapon : Item
    {
        [Required]
        public int Damage { get; set; }

        [Required]
        public int MissRate { get; set; }


        public override string ToString()
        {
            return $"{Description}";
        }
    }
}