﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace DataAccessLayer.Context
{
    public class DbContextFactory : DesignTimeDbContextFactoryBase<ZorkDbContext>
    {
        protected override ZorkDbContext CreateNewInstance(DbContextOptions<ZorkDbContext> options)
        {
            return new ZorkDbContext(options);
        }
    }
}