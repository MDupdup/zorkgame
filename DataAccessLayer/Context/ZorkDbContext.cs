﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Context
{
    public class ZorkDbContext : DbContext
    {
        public DbSet<Player> Players { get; set; }
        public DbSet<Weapon> Weapons { get; set; }
        public DbSet<Monster> Monsters { get; set; }
        public DbSet<Cell> Cells { get; set; }
        public DbSet<Bonus> Bonuses { get; set; }

        public ZorkDbContext(DbContextOptions<ZorkDbContext> options) : base(options)
        {
        }
    }
}