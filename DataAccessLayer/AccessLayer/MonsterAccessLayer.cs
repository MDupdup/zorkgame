﻿using System;
using System.Linq;
using DataAccessLayer.Context;
using DataAccessLayer.Models;

namespace DataAccessLayer.AccessLayer
{
    public class MonsterAccessLayer : BaseAccessLayer<Monster>
    {
        public MonsterAccessLayer(ZorkDbContext context) : base(context) {}

        public void SaveMonsterState()
        {
            _context.SaveChanges();
        }
        
        public Monster SelectRandomMonster()
        {
            var rdm = new Random();
            var toSkip = rdm.Next(_context.Monsters.Count());
            return _context.Monsters.Skip(toSkip).Take(1).First();
        }
    }
}