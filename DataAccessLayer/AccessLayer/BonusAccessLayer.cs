﻿using System;
using System.Linq;
using DataAccessLayer.Context;
using DataAccessLayer.Models;

namespace DataAccessLayer.AccessLayer
{
    public class BonusAccessLayer : BaseAccessLayer<Bonus>
    {
        public BonusAccessLayer(ZorkDbContext context) : base(context) {}
        
        public Bonus SelectRandomBonus()
        {
            var rdm = new Random();
            var toSkip = rdm.Next(_context.Bonuses.Count());
            return _context.Bonuses.Skip(toSkip).Take(1).First();
        }
        
        public void SaveBonusState()
        {
            _context.SaveChanges();
        }
    }
}