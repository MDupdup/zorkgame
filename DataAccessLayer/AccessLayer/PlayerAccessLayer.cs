﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.Context;
using DataAccessLayer.Models;

namespace DataAccessLayer.AccessLayer
{
    public class PlayerAccessLayer : BaseAccessLayer<Player>
    {
        public PlayerAccessLayer(ZorkDbContext context) : base(context) {}

        public Player GetPlayer()
        {
            return _context.Players.FirstOrDefault();
        }

        public void SavePlayerState()
        {
            _context.SaveChanges();
        }

        public async void UpdateLife(int newHp)
        {
            GetPlayer().Hp = newHp;
            await _context.SaveChangesAsync();
        }

        public async Task ChangeWeapon(Weapon newWeapon)
        {
            GetPlayer().Weapon = newWeapon;
            await _context.SaveChangesAsync();
        }
        
        public async Task AddBonus(Bonus newBonus)
        {
            GetPlayer().Bonuses ??= new List<Bonus>();
            GetPlayer().Bonuses.Add(newBonus);
            await _context.SaveChangesAsync();
        }

        public void ResetItems()
        {
            GetPlayer().Weapon = null;
            GetPlayer().Bonuses = null;

            _context.SaveChanges();
        }

        // public new async Task<int> RemoveAsync(int id)
        // {
        //     await _b.EmptyTable();
        //     await _weaponAccessLayer.EmptyTable();
        //
        //     await base.RemoveAsync(id);
        // }
    }
}