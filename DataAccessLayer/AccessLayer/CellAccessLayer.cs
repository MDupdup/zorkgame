﻿using DataAccessLayer.Context;
using DataAccessLayer.Models;
using System.Linq;
using System.Linq.Expressions;

namespace DataAccessLayer.AccessLayer
{
    public class CellAccessLayer : BaseAccessLayer<Cell>
    {
        public CellAccessLayer(ZorkDbContext context) : base(context) {}

        public Cell GetCellByCoords(int xCoord, int yCoord)
        {
            return _context.Cells.First(c => c.PosX == xCoord && c.PosY == yCoord);
        }

        public string GetWinningCellDirection(Cell cell)
        {
            var winningCell = _context.Cells.First(c => c.IsWinningCell);
            
            if (winningCell.PosX > cell.PosX)
                if (winningCell.PosY > cell.PosY) return "north-east";
                else return "south-east";
            else 
                if (winningCell.PosY > cell.PosY) return "north-west";
                else return "north-west";
        }
        
        public void SaveCellState()
        {
            _context.SaveChanges();
        }
    }
}