﻿using System;
using System.Linq;
using DataAccessLayer.Context;
using DataAccessLayer.Models;

namespace DataAccessLayer.AccessLayer
{
    public partial class WeaponAccessLayer : BaseAccessLayer<Weapon>
    {
        public WeaponAccessLayer(ZorkDbContext context) : base(context) {}
        
        public Weapon SelectRandomWeapon()
        {
            var rdm = new Random();
            var toSkip = rdm.Next(_context.Weapons.Count());
            return _context.Weapons.Skip(toSkip).Take(1).First();
        }
        
        public void SaveWeaponState()
        {
            _context.SaveChanges();
        }
    }
}