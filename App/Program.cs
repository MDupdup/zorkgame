﻿using System;
using App.Game;
using DataAccessLayer.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StudentManager.DataAccessLayer.Extensions;

namespace App
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            using var host = CreateHostBuilder(args).Build();
            var run = host.RunAsync();
            
            var engine = host.Services.GetService<Engine>();
            
            // launcher.Exit += (o,e) => host.StopAsync();
            
            engine?.Start();
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();
            
            return Host.CreateDefaultBuilder(args)
                .ConfigureServices((_, services) =>
                {
                    services.AddDataAccessLayerService(configuration);
                    services.AddSingleton<Engine>();
                });
        }
            
    }
}