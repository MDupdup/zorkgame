﻿using System;
using System.Collections.Generic;
using System.IO;
using DataAccessLayer.Models;
using Microsoft.Extensions.Configuration;

namespace App.Game
{
    public static class Utils
    {
        public static int GetGridSize()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            IConfiguration config = builder.Build();

            return int.Parse(config["Variables:GridSize"]);
        }

        // plus de chance d'avoir entre 0 et 50
        public static int CalculateMonsterSpawnRate()
        {
            var rdm50 = new Random().Next(50);
            var rdm502 = new Random().Next(50);
            var rdm100 = new Random().Next(100);

            return (rdm50 + rdm502 + rdm100) / 3;
        }

        public static bool DoMonsterSpawn(int spawnChance)
        {
            var rdm = new Random();
            return rdm.Next(100) < spawnChance;
        }

        public static bool DoPlayerFlee(Player player, Monster monster)
        {
            var rdm = new Random();

            return rdm.Next(100) < 50 + player.Level;
        }

        public static bool DoItemSpawn()
        {
            var rdm = new Random();
            return rdm.Next(100) >= 85;
        }

        public static bool DoHintSpawn()
        {
            var rdm = new Random();
            return rdm.Next(100) >= 64;
        }

        private static int CalculateDamage(double atk)
        {
            var ratio = 15;
            var min = atk - Math.Round(atk / 100 * ratio);
            var max = atk + Math.Round(atk / 100 * ratio);
            var values = new List<double>();
            for (var i = min; i <= max; i++)
                values.Add(i);

            var rdm = new Random();
            return Convert.ToInt32(values[rdm.Next(values.Count)]);
        }

        public static int CalculatePlayerDamage(Player player)
        {
            var atk = player.BaseAtk + player.Level * 1.25 + (player.Weapon?.Damage ?? 0);
            var rdm = new Random();

            Console.WriteLine("Bonjnour");

            return rdm.Next(100) < (player.Weapon?.MissRate ?? 100) ? CalculateDamage(atk) : 0;
        }

        public static int CalculateMonsterDamage(Monster monster)
        {
            var atk = Convert.ToInt32(monster.Attack);
            var rdm = new Random();

            return rdm.Next(100) > monster.MissRate ? CalculateDamage(atk) : 0;
        }
    }
}