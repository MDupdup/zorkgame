﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.Models;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace App.Game
{
    public static class MenusHandler
    {
        public static string MainMenu()
        {
            string x;
            var validInputs = new[] {"1", "2", "3", "4"};

            do
            {
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Blue;
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine("#############################################################################");
                Console.WriteLine("#                             Welcome to ZORG                               #");
                Console.WriteLine("#############################################################################");

                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;

                
                Console.WriteLine("MAIN MENU:");
                Console.WriteLine("- Create New Game    (1)");
                Console.WriteLine("- Load Save Game     (2)");
                Console.WriteLine("- About              (3)");
                Console.WriteLine("- Exit               (4)");
                Console.Write(">");


                x = Console.ReadLine();
                try
                {
                    if (validInputs.Contains(x))
                        return x;
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Value read = {1}.", e.Message, x);
                }
            } while (!validInputs.Contains(x));

            return "";
        }


        public static string AskForName()
        {
            Console.Clear();
            
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                             CREATE NEW GAME                               #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            string x;

            do
            {
                // TODO si y'a déjà une partie on demande si on la supprime
                Console.WriteLine("Hello Adventurer! What is your name?");
                Console.Write(">");

                x = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(x)) continue;
                    Console.WriteLine($"Let's go {x}!");
                    return x;
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Value read = {1}.", e.Message, x);
                }
            } while (string.IsNullOrEmpty(x));

            return "";
        }

        public static bool AskToResetGame(Player player)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                             !!! WARNING !!!                               #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            ConsoleKey x;
            do
            {
                Console.WriteLine($"\nYou are about to overwrite your precedent game:" +
                                  $"\n{player}" +
                                  $"\n\nAre you sure? (Y/n)");
                Console.Write(">");

                x = Console.ReadKey().Key;
                try
                {
                    if (x == ConsoleKey.Y || x == ConsoleKey.Enter)
                        return true;
                    else if (x == ConsoleKey.N)
                        return false;
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Value read = {1}.", e.Message, x);
                }
            } while (!new[] {"y", "n"}.Contains(x.ToString().ToLower()));

            return false;
        }

        public static bool LoadSavedGame(Player player)
        {
            if (player == null)
            {
                Console.WriteLine("No game was found!");
                Console.WriteLine("\nPress any key to continue...");
                Console.Read();
                return false;
            }

            ConsoleKey x;

            do
            {
                Console.WriteLine($"Here is your saved game: {player}");
                Console.WriteLine("Do you want to continue your game? (Y/n)");
                Console.Write(">");

                x = Console.ReadKey().Key;
                try
                {
                    if (x == ConsoleKey.Y || x == ConsoleKey.Enter)
                        return true;
                    else if (x == ConsoleKey.N)
                        return false;
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Value read = {1}.", e.Message, x);
                }
            } while (!new[] {"y", "n"}.Contains(x.ToString().ToLower()));

            return false;
        }

        public static void About()
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                                About ZORG                                 #");
            Console.WriteLine("#############################################################################");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Project made by DUPONT Malo and CAUMES Kirian in M2 Ynov Nantes\n");

            // Console.BackgroundColor = ConsoleColor.Red;
            // Console.ForegroundColor = ConsoleColor.White;
            // Console.WriteLine("#############################################################################");
            // Console.WriteLine("#                               Command List                                #");
            // Console.WriteLine("#############################################################################");
            // Console.BackgroundColor = ConsoleColor.Black;
            // Console.ForegroundColor = ConsoleColor.White;
            // Console.WriteLine("go [north/n, east/e, west/w, south/s]:   Move to");
            // Console.WriteLine("inventory/i:                             Move to");
            // Console.WriteLine("diagnostic/d:                            See your stats");
            // Console.WriteLine("pikcup/p \"item name\":                    Pickup an item on the floor\n");

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                   Easter Eggs Commands (in-game menu)                     #");
            Console.WriteLine("#############################################################################");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("uuddlrlrba:                              Konami Code (Make your hp full)");
            Console.WriteLine("kill:                                    Commit suicide");
            Console.WriteLine("win:                                     Win the game");

            Console.Write("Press any key to return to the main menu...");
            Console.ReadKey();
        }

        public static string StartPlayerTurn()
        {
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                             It's your turn!                               #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            string x;
            var validInputs = new[] {"1", "2", "3", "4"};

            do
            {
                Console.WriteLine("- Display your stats (1)");
                Console.WriteLine("- Describe room      (2)");
                Console.WriteLine("- Move               (3)");
                Console.WriteLine("- Save and Exit      (4)");
                Console.Write(">");

                x = Console.ReadLine();
                try
                {
                    if (validInputs.Contains(x))
                        return x;
                    
                    // Konami code
                    if (x!.ToLower() == "uuddlrlrba")
                        return "konami";
                    if (x!.ToLower() == "kill")
                        return "kill";
                    if (x!.ToLower() == "win")
                        return "win";
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Value read = {1}.", e.Message, x);
                }
            } while (!validInputs.Contains(x));

            return "";
        }

        // public static string SelectWeapon(List<Weapon> weapons)
        // {
        //     var xInt = 0;
        //     do
        //     {
        //         Console.WriteLine("Which weapon do you want to use?");
        //         foreach (var weapon in weapons)
        //         {
        //             Console.WriteLine(
        //                 $"- {weapon.Name} (atk: {weapon.Damage}/miss: {weapon.MissRate})   ({weapons.IndexOf(weapon) + 1})");
        //         }
        //
        //         Console.WriteLine($"- Exit               ({weapons.Count + 1})");
        //         Console.Write(">");
        //
        //         var x = Console.ReadLine();
        //         try
        //         {
        //             xInt = int.Parse(x ?? string.Empty);
        //
        //             if (xInt >= 1 && xInt <= weapons.Count + 1)
        //                 return x;
        //         }
        //         catch (Exception e)
        //         {
        //             Console.WriteLine("{0} Value read = {1}.", e.Message, x);
        //         }
        //     } while (xInt < 1 || xInt > weapons.Count + 1);
        //
        //     return "";
        // }

        public static void DisplayStats(Player player)
        {
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                                Your Stats                                 #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine(player);

            Console.WriteLine("\nYour bonuses:");

            if (player.Bonuses == null || player.Bonuses.Count == 0)
                Console.WriteLine("You don't have any, loser");
            else
                foreach (var (item, i) in player.Bonuses.Select((v, i) => (v, i)))
                    Console.WriteLine($"{i + 1}. {item.Name}: {item}");

            Console.WriteLine("\nYour weapon:");
            Console.WriteLine(player.Weapon == null
                ? "You don't have any weapon"
                : $"{player.Weapon.Name}: {player.Weapon}");

            Console.Write("\nPress any key to go back...");
            Console.ReadLine();
        }

        public static Bonus UseItem(Player player)
        {
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                                Your Items                                 #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            if (player.Bonuses == null || player.Bonuses.Count == 0)
            {
                Console.WriteLine("You don't have any bonus item!");
                Console.Write("\nPress any key to go back...");
                Console.ReadLine();
                return null;
            }

            var xInt = 0;
            do
            {
                Console.WriteLine("Which weapon do you want to use?");
                var i = 1;
                foreach (var bonus in player.Bonuses)
                {
                    var bonusSpecs = "";

                    if (bonus.AtkGain > 0)
                        bonusSpecs = $"gives {bonus.AtkGain} atk";
                    if (bonus.DefGain > 0)
                        bonusSpecs = $"gives {bonus.DefGain} def";
                    if (bonus.HpGain > 0)
                        bonusSpecs = $"gives {bonus.HpGain} Hp";

                    Console.WriteLine($"- {bonus.Name}: {bonus.Description} ({bonusSpecs}) ({i})");
                    i++;
                }

                Console.WriteLine($"- Exit               ({player.Bonuses.Count + 1})");
                Console.Write(">");

                var x = Console.ReadLine();
                try
                {
                    xInt = int.Parse(x ?? string.Empty);

                    if (xInt >= 1 && xInt <= player.Bonuses.Count + 1)
                        return player.Bonuses.ToList()[xInt - 1];
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Value read = {1}.", e.Message, x);
                }
            } while (xInt < 1 || xInt > player.Bonuses.Count + 1);

            return null;
        }

        public static void DescribeRoom(Cell cell)
        {
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                            Room Description                               #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine(cell);

            Console.Write("\nPress any key to go back...");
            Console.ReadLine();
        }

        public static string Move(Cell currentCell)
        {
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                             It's your turn!                               #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            var gridSize = Utils.GetGridSize();
            var prohibitedDests = "";
            if (currentCell.PosX == 0)
                prohibitedDests += "w";
            if (currentCell.PosY == 0)
                prohibitedDests += "n";
            if (currentCell.PosX == gridSize - 1)
                prohibitedDests += "e";
            if (currentCell.PosY == gridSize - 1)
                prohibitedDests += "s";

            string x;
            var validInputs = new[] {"1", "2", "3", "4"};
            var directions = new Dictionary<string, string>
            {
                {"w", "West, of course"},
                {"n", "North, for sure"},
                {"e", "East, obviously"},
                {"s", "South, let's go"}
            };
            var resultSet = new Dictionary<int, string>();

            do
            {
                Console.WriteLine("Which way do you want to go?");

                var i = 1;

                foreach (KeyValuePair<string, string> entries in directions)
                {
                    if (prohibitedDests.Contains(entries.Key)) continue;
                    Console.WriteLine($"{entries.Value}      ({i})");
                    resultSet[i] = entries.Key;
                    i++;
                }

                Console.WriteLine($"Go back.             ({i})");
                Console.Write(">");

                x = Console.ReadLine();
                try
                {
                    if (int.Parse(x!) == resultSet.Count + 1) return "q";
                    if (resultSet.Keys.Contains(int.Parse(x)))
                        return resultSet[int.Parse(x!)];
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Value read = {1}.", e.Message, x);
                }
            } while (!resultSet.Values.Contains(x));

            return "";
        }

        private static void DisplayMonster(Monster monster)
        {
            var fWords = new[] {"F*ck", "Sh*t", "Damn", "Oh god"};
            var rdm = new Random();

            Console.BackgroundColor = ConsoleColor.Yellow;

            Console.WriteLine("#############################################################################");
            Console.WriteLine(
                $"#                {fWords[rdm.Next(fWords.Length)]}, the monster {monster.Name} appears!                   #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;
        }

        public static string FightMenu(Player player, Monster monster)
        {
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;

            DisplayMonster(monster);

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            string x;
            var validInputs = new[] {"1", "2", "3", "4"};

            do
            {
                Console.WriteLine("What do you want to do?");
                Console.WriteLine("- Display Inventory  (1)");
                Console.WriteLine("- Use an Item        (2)");
                Console.WriteLine("- Attack             (3)");
                Console.WriteLine("- Run away           (4)");
                Console.Write(">");

                x = Console.ReadLine();
                try
                {
                    if (validInputs.Contains(x))
                        return x;
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Value read = {1}.", e.Message, x);
                }
            } while (!validInputs.Contains(x));

            return "";
        }

        public static void AttackMonster(Player player, Monster monster, int playerLifeLost, int monsterLifeLost)
        {
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                                   FIGHT!                                  #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            var weapon = "";
            weapon = player.Weapon != null ? $" his {player.Weapon.Name}" : "his bare hands";

            Console.WriteLine($"{player.Name} attacks {monster.Name} with {weapon}!");
            Console.WriteLine($"{monster.Name} takes {monsterLifeLost} damage! It now has {monster.Hp}HP.");
            Console.WriteLine($"{monster.Name} attacks {player.Name}!");
            Console.WriteLine($"{player.Name} takes {playerLifeLost} damage! He now has {player.Hp}HP.");

            Console.WriteLine("\nPress any key to continue...");
            Console.ReadLine();
        }

        public static void GetAttacked(Player player, Monster monster, int playerLifeLost)
        {
            Console.WriteLine("\nYou failed to run away!");
            Console.WriteLine($"{monster.Name} attacks {player.Name}!");
            Console.WriteLine($"{player.Name} takes {playerLifeLost} damage! He now has {player.Hp}HP.");

            Console.WriteLine("\nPress any key to continue...");
            Console.ReadLine();
        }

        public static void MonsterKilled(int xp)
        {
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Green;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("#############################################################################");
            Console.WriteLine("                                 WELL PLAYED                                #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine($"Congrats! You defeated the monster. You gained {xp}XP points!");

            Console.WriteLine("\nPress any key to continue...");
            Console.ReadLine();
        }

        public static void GameOver(Player player)
        {
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                                GAME OVER!                                 #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;

            Console.WriteLine("Too bad! You failed :( Be better next time!");
            Console.WriteLine("Your stats:");
            Console.WriteLine(player);

            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("\nPress any key to continue...");
            Console.ReadLine();
        }

        public static void WinScreen(Player player)
        {
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Green;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("#############################################################################");
            Console.WriteLine("#                            CONGRATULATIONS!                               #");
            Console.WriteLine("#############################################################################");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine("Well played, you found the Door, and exited the dungeon!");
            Console.WriteLine("Your stats:");
            Console.WriteLine(player);

            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("\nPress any key to continue...");
            Console.ReadLine();
        }

        public static bool GetNewItem(Player player, Item item)
        {
            ConsoleKey x;

            do
            {
                Console.WriteLine("You found an item laying on the floor! do you want to take it? (Y/n)");
                Console.WriteLine($"Item: {item}");

                if (item is Bonus && player.Bonuses != null && player.Bonuses.Count > 0)
                {
                    Console.WriteLine("Your actual items:");
                    var i = 1;
                    foreach (var bonus in player.Bonuses)
                    {
                        Console.WriteLine($"{i}. {bonus}");
                    }
                }
                else if (item is Weapon && player.Weapon != null)
                {
                    Console.WriteLine("Your actual weapon:");
                    Console.WriteLine($"{player.Weapon}");
                }
                
                Console.Write(">");

                
                x = Console.ReadKey().Key;
                try
                {
                    if (x == ConsoleKey.Y || x == ConsoleKey.Enter)
                        return true;
                    else if (x == ConsoleKey.N)
                        return false;
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Value read = {1}.", e.Message, x);
                }
            } while (!new[] {"y", "n"}.Contains(x.ToString().ToLower()));

            return false;
        }

        public static void DisplayHint(string direction)
        {
            Console.WriteLine("You found an old piece of paper! Something is written...\n");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.WriteLine("|                                                        |");
            Console.WriteLine("|                                                        |");
            Console.WriteLine($"|          The exit is {direction} from here!            |");
            Console.WriteLine("|                                                        |");
            Console.WriteLine("|                                                        |");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            Console.WriteLine("\nPress any key to continue...");
            Console.ReadLine();        
        }        
        
        public static bool AskToQuit()
        {
            ConsoleKey x;

            do
            {
                Console.WriteLine("Are you sure you want to quit the game? (Y/n)");
                Console.Write(">");

                x = Console.ReadKey().Key;
                try
                {
                    if (x == ConsoleKey.Y || x == ConsoleKey.Enter)
                        return true;
                    else if (x == ConsoleKey.N)
                        return false;
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Value read = {1}.", e.Message, x);
                }
            } while (!new[] {"y", "n"}.Contains(x.ToString().ToLower()));

            return false;
        }
    }
}