﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.AccessLayer;
using DataAccessLayer.Models;
using Microsoft.Extensions.Configuration;

namespace App.Game
{
    public class Engine
    {
        private readonly PlayerAccessLayer _playerAccessLayer;
        private readonly CellAccessLayer _cellAccessLayer;
        private readonly MonsterAccessLayer _monsterAccessLayer;
        private readonly WeaponAccessLayer _weaponAccessLayer;
        private readonly BonusAccessLayer _bonusAccessLayer;

        private readonly IConfiguration _config;

        public Engine(
            PlayerAccessLayer playerAccessLayer,
            CellAccessLayer cellAccessLayer,
            MonsterAccessLayer monsterAccessLayer,
            WeaponAccessLayer weaponAccessLayer,
            BonusAccessLayer bonusAccessLayer,
            IConfiguration config
        )
        {
            _playerAccessLayer = playerAccessLayer;
            _cellAccessLayer = cellAccessLayer;
            _monsterAccessLayer = monsterAccessLayer;
            _weaponAccessLayer = weaponAccessLayer;
            _bonusAccessLayer = bonusAccessLayer;
            _config = config;
        }

        public async void Start()
        {
            var player = _playerAccessLayer.GetPlayer();

            Console.Clear();
            switch (MenusHandler.MainMenu())
            {
                case "1":
                    Console.Clear();
                    if (player != null)
                        if (!MenusHandler.AskToResetGame(player))
                            Start();
                        else
                        {
                            await _bonusAccessLayer.EmptyTable();
                            await _weaponAccessLayer.EmptyTable();
                            await _playerAccessLayer.RemoveAsync(player.Id);
                            _playerAccessLayer.SavePlayerState();

                        }

                    var name = MenusHandler.AskForName();
                    await InitGame(name);
                    await RunGame();
                    break;
                case "2":
                    Console.Clear();
                    if (MenusHandler.LoadSavedGame(player))
                        await RunGame();
                    else
                        Start();
                    break;
                case "3":
                    Console.Clear();
                    MenusHandler.About();
                    Start();
                    break;
                case "4":
                    Console.Clear();
                    Console.WriteLine("Goodbye!");
                    Environment.Exit(1);
                    break;
            }
        }

        private async Task InitGame(string name)
        {
            await _playerAccessLayer.EmptyTable();
            await _cellAccessLayer.EmptyTable();
            await _bonusAccessLayer.EmptyTable();
            await _weaponAccessLayer.EmptyTable();
            _playerAccessLayer.SavePlayerState();


            var cells = GenerateCells(Utils.GetGridSize(), Utils.GetGridSize());

            // Relancer le placement dans le cas ou on arrive sur la cell gagnante
            var rdm = new Random();
            Cell randomSpawningCell;
            do randomSpawningCell = cells[rdm.Next(cells.Count)];
            while (randomSpawningCell.IsWinningCell);

            await _playerAccessLayer.AddAsync(new Player
            {
                Name = name,
                MaxHp = 100,
                Hp = 100,
                Xp = 0,
                BaseAtk = 10,
                BaseDef = 10,
                CellX = randomSpawningCell.PosX,
                CellY = randomSpawningCell.PosY,
                MonsterKilled = 0
            });

            await _cellAccessLayer.AddRangeAsync(cells);
            _cellAccessLayer.SaveCellState();

            await InitGameEntities();
        }

        private async Task InitGameEntities()
        {
            // Weapons
            if (_weaponAccessLayer.GetCollection().ToList().Count == 0)
            {
                await _weaponAccessLayer.AddRangeAsync(new List<Weapon>
                    {
                        new()
                        {
                            Name = "Sword of Altaïr",
                            Description = 
                                "The Sword of Altaïr was the personal sword of significant power wielded by Altaïr Ibn-La'Ahad through much of his life as an Assassin.",
                            Damage = 28,
                            MissRate = 9
                        },
                        new()
                        {
                            Name = "The Sweard",
                            Description = "The Sweard is a weird weapon that insults its victims.",
                            Damage = 19,
                            MissRate = 16
                        },
                        new()
                        {
                            Name = "Real zippo of Papa Johnny",
                            Description = "\"Allumez le feu !\"",
                            Damage = 32,
                            MissRate = 3
                        }
                    });

                _weaponAccessLayer.SaveWeaponState();
            }

            // Monsters
            if (_monsterAccessLayer.GetCollection().ToList().Count == 0)
            {
                await _monsterAccessLayer.AddRangeAsync(new List<Monster>
                {
                    new()
                    {
                        Name = "Dark magician",
                        Description = "Stories tell that dark magicians cannot procreate",
                        Attack = 36,
                        HpMax = 82,
                        Hp = 82,
                        MissRate = 18
                    },
                    new()
                    {
                        Name = "Red",
                        Description = "...",
                        Attack = 43,
                        HpMax = 67,
                        Hp = 67,
                        MissRate = 2
                    },
                    new()
                    {
                        Name = "Zinedine Zidane",
                        Description = "Watch out for his coup de boule!",
                        Attack = 24,
                        HpMax = 33,
                        Hp = 33,
                        MissRate = 1
                    },
                    new()
                    {
                        Name = "Xana",
                        Description = "A world without danger",
                        Attack = 28,
                        HpMax = 50,
                        Hp = 50,
                        MissRate = 4
                    }
                });
                
                _monsterAccessLayer.SaveMonsterState();
            }

            // Items
            if (_bonusAccessLayer.GetCollection().ToList().Count == 0)
            {
                await _bonusAccessLayer.AddRangeAsync(new List<Bonus>
                {
                    new()
                    {
                        Name = "WHEY",
                        Description = "A potion created by Tibo inShape in person (boosts atk)",
                        AtkGain = 34,
                        DefGain = 0,
                        HpGain = 0
                    },
                    new()
                    {
                        Name = "Umbrella (strong one)",
                        Description = "A very strong and manly umbrella to protect you from reality (boosts def)",
                        AtkGain = 0,
                        DefGain = 50,
                        HpGain = 0
                    },
                    new()
                    {
                        Name = "Tagada strawberry",
                        Description = "Soooo cute (restores HP)",
                        AtkGain = 0,
                        DefGain = 0,
                        HpGain = 100
                    }
                });

                _bonusAccessLayer.SaveBonusState();
            }
        }

        private static List<Cell> GenerateCells(int xLen = 10, int yLen = 10)
        {
            var rnd = new Random();
            var exitCoordX = rnd.Next(xLen + 1);
            var exitCoordY = rnd.Next(yLen + 1);
            
            // TODO faire un loading? 

            var descriptions = new[]
            {
                "It is a beautiful golden room.",
                "You enter a green plain with some trees",
                "Ouch, there is lava all over the place",
                "What a nice kebab restaurant!",
                "Wait... is that your bedroom?",
            };

            var cells = new List<Cell>();

            for (var y = 0; y < xLen; y++)
            {
                for (var x = 0; x < yLen; x++)
                {
                    var isWinningCell = x == exitCoordX && y == exitCoordY;

                    cells.Add(new Cell
                    {
                        PosX = x,
                        PosY = y,
                        MonsterSpawnRate = Utils.CalculateMonsterSpawnRate(),
                        IsWinningCell = isWinningCell,
                        Description = isWinningCell
                            ? "This is definitely the way out!"
                            : descriptions[rnd.Next(descriptions.Length)]
                    });
                }
            }

            return cells;
        }

        private async Task RunGame()
        {
            var player = _playerAccessLayer.GetPlayer();
            var running = true;

            while (running)
            {
                var currentCell = _cellAccessLayer.GetCellByCoords(player.CellX, player.CellY);

                switch (MenusHandler.StartPlayerTurn())
                {
                    case "1":
                        MenusHandler.DisplayStats(player);
                        break;
                    case "2":
                        MenusHandler.DescribeRoom(currentCell);
                        break;
                    case "3":
                        var moveTo = MenusHandler.Move(currentCell);
                        var newPlayerCell = MakePlayerMove(player, moveTo);

                        if (newPlayerCell == null) continue;
                        if (newPlayerCell.IsWinningCell)
                        {
                            MenusHandler.WinScreen(player);
                            await _bonusAccessLayer.EmptyTable();
                            await _weaponAccessLayer.EmptyTable();
                            await _playerAccessLayer.RemoveAsync(player.Id);
                            _playerAccessLayer.SavePlayerState();
                            Environment.Exit(1);
                        }
                        
                        // Spawn des monstres
                        if (newPlayerCell.Monster != null && newPlayerCell.Monster.Hp > 0)
                        {
                            await Fight(player, newPlayerCell);
                        }
                        else if (Utils.DoMonsterSpawn(newPlayerCell.MonsterSpawnRate))
                        {
                            var monster = _monsterAccessLayer.SelectRandomMonster();
                            monster.Hp = monster.HpMax;
                            newPlayerCell.Monster = monster;

                            await Fight(player, newPlayerCell);
                        }

                        // Spawn des bonus
                        if (Utils.DoItemSpawn())
                        {
                            var newBonus = _bonusAccessLayer.SelectRandomBonus();

                            if (MenusHandler.GetNewItem(player, newBonus))
                            {
                                await _playerAccessLayer.AddBonus(newBonus);

                            }
                        }

                        // Spawn des armes
                        if (Utils.DoItemSpawn())
                        {
                            var newWeapon = _weaponAccessLayer.SelectRandomWeapon();

                            if (MenusHandler.GetNewItem(player, newWeapon))
                            {
                                await _playerAccessLayer.ChangeWeapon(newWeapon);

                            }
                        }

                        if (Utils.DoHintSpawn())
                            MenusHandler.DisplayHint(_cellAccessLayer.GetWinningCellDirection(currentCell));

                        break;
                    case "4":
                        if (MenusHandler.AskToQuit())
                        {
                            running = false;
                            Start();
                        }

                        break;
                    case "konami":
                        Console.WriteLine("You entered the konami code...");
                        Console.WriteLine("You are now full HP!");
                        Console.WriteLine("\nPress any key to continue...");
                        Console.ReadLine();                        
                        player.Hp = player.MaxHp;
                        _playerAccessLayer.SavePlayerState();
                        break;
                    case "kill":
                        Console.WriteLine("You entered the kill code...");
                        Console.WriteLine("You are now dead!");
                        player.Hp = 0;
                        _playerAccessLayer.SavePlayerState();

                        MenusHandler.GameOver(player);
                        Environment.Exit(1);
                        break;
                    case "win":
                        Console.WriteLine("You entered the win code...");
                        Console.WriteLine("You have won the game!");
                        await _playerAccessLayer.RemoveAsync(player.Id);
                        _playerAccessLayer.SavePlayerState();
                        MenusHandler.WinScreen(player);
                        Environment.Exit(1);
                        break;
                }
            }
        }

        private Cell MakePlayerMove(Player player, string moveTo)
        {
            switch (moveTo)
            {
                case "w":
                    // West
                    player.CellX -= 1;
                    break;
                case "n":
                    // North
                    player.CellY -= 1;
                    break;
                case "e":
                    // East
                    player.CellX += 1;
                    break;
                case "s":
                    // South
                    player.CellY += 1;
                    break;
                case "q":
                    // Return
                    return null;
            }
            _playerAccessLayer.SavePlayerState();
            
            return _cellAccessLayer.GetCellByCoords(player.CellX, player.CellY);
        }

        private async Task Fight(Player player, Cell cell)
        {
            do
            {
                // menu de combat
                // calcul de la fuite
                // utilisation de potions
                // afficher les stats des degats
                
                switch (MenusHandler.FightMenu(player, cell.Monster))
                {
                    case "1":
                        MenusHandler.DisplayStats(player);
                        break;
                    case "2":
                        var bonus = MenusHandler.UseItem(player);
                        if (bonus != null)
                        {
                            player.BaseAtk += bonus.AtkGain;
                            player.BaseDef += bonus.DefGain;
                            player.Hp += bonus.HpGain;
                            if (player.Hp > player.MaxHp) player.Hp = player.MaxHp;
                            player.Bonuses.Remove(bonus);
                            
                            _playerAccessLayer.SavePlayerState();
                            
                            Console.WriteLine("You upgraded your stats!");
                            Console.WriteLine(player);
                            Console.Write("\nPress any key to go back...");
                            Console.ReadLine();
                        }
                        break;
                    case "3":
                    {
                        // Calcul des dégâts
                     
                        //var monsterDamage = Utils.CalculateMonsterDamage(cell.Monster) - player.BaseDef;
                        //var playerLifeLost = monsterDamage > 0 ? monsterDamage : 0;

                        var monsterDamage = Utils.CalculateMonsterDamage(cell.Monster) - player.BaseDef;
                        var playerLifeLost = monsterDamage > 0 ? monsterDamage : 0;
                        var playerDamage = Utils.CalculatePlayerDamage(player) - cell.Monster.Defense;
                        var monsterLifeLost = playerDamage > 0 ? playerDamage : 0;
                        player.Hp -= playerLifeLost;
                        cell.Monster.Hp -= monsterLifeLost;
                        
                        MenusHandler.AttackMonster(player, cell.Monster, playerLifeLost, monsterLifeLost);
                        _playerAccessLayer.SavePlayerState();
                        break;
                    }
                    case "4":
                    {
                        if (Utils.DoPlayerFlee(player, cell.Monster))
                        {
                            Console.WriteLine("\nYou managed to run away!");
                            Console.WriteLine("\nPress any key to continue...");
                            Console.ReadLine();
                            return;
                        }
                        else
                        {
                            var monsterDamage = Utils.CalculateMonsterDamage(cell.Monster) - player.BaseDef;
                            var playerLifeLost = monsterDamage > 0 ? monsterDamage : 0;
                            player.Hp -= playerLifeLost;
                            MenusHandler.GetAttacked(player, cell.Monster, playerLifeLost);
                            
                            _playerAccessLayer.SavePlayerState();
                        }
                        break;
                    }
                        
                }

            } while (player.Hp > 0 && cell.Monster.Hp > 0);

            if (cell.Monster.Hp <= 0)
            {
                var xpGained = Convert.ToInt32(cell.Monster.HpMax * 0.5);
                player.Xp += xpGained;
                player.MonsterKilled++;
                MenusHandler.MonsterKilled(xpGained);
                
                _playerAccessLayer.SavePlayerState();
            }
            if (player.Hp <= 0)
            {
                MenusHandler.GameOver(player);
                await _playerAccessLayer.RemoveAsync(player.Id);
                _playerAccessLayer.SavePlayerState();
                Environment.Exit(1);
            }
        }
    }
}